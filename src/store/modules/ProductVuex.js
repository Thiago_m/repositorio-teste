// import Api from '../../api/index'

const Product = {
    namespaced: true,
    state: {
      allProducts: null, // [],
  
      newProducts: null, // [],
  
      oneProduct: null, // {}
  
      success: null,
      success_msg: null,
      error: null,
      error_msg: null,
    },
    getters: {
      getAllProducts: state => {
        return state.allProducts;
      }
    },
  
    mutations: {
      setOneItem(state, item){
        state.allProducts.push(item) // item.data
        localStorage.setItem('Products', JSON.stringify(state.allProducts))
      },
      setAllItems(state, items){
        state.allProducts = items // items.data
        localStorage.setItem('Products', JSON.stringify(state.allProducts))
      },
      deleteOneItem(state, item) {
        let index = state.allProducts.indexOf(item)
        state.allProducts.splice(index, 1)
        let aux_storage = JSON.parse(localStorage.getItem('Products'))
        aux_storage.splice(index, 1)
        localStorage.setItem('Products', JSON.stringify(aux_storage))
      },
      editOneItem(state, item) {
        let index = state.allProducts.indexOf(item)
        state.allProducts.splice(index, 1)
        let aux_storage = JSON.parse(localStorage.getItem('Products'))
        aux_storage.splice(index, 1)
        localStorage.setItem('Products', JSON.stringify(aux_storage))
      }
    },
  
    actions: {

      addOneItem(context, item) {
        context.commit('setOneItem', item)
      },
      deleteOneProduct(context, item) {
        context.commit('deleteOneItem', item)
      },
      editOneProduct(context, item) {
        context.commit('editOneItem', item)
      },
  
      // index, show, create, update, destroy/delete
  
      // index => listar todos do mesmo do MODEL
      // show => listar apenas 1 do model 
      // create => criar um novo produto, ou entidade do model
      // update => atualizar uma entidade do model
      // destroy/delete => deletar
  
  
      index(context){
        let products_local_storage = JSON.parse(localStorage.getItem('Products'))
        let products_no_banco_de_dados = [
          {name: 'Queijo',  value: 10,category:'aa', approved: false},
          {name: 'Bacon',   value: 10,category:'aa', approved: false},
          {name: 'Batata',  value: 10,category:'aa', approved: false},
          {name: 'Maçã',    value: 10,category:'aa', approved: false},
        ]

        if (products_local_storage == null) {
          context.commit('setAllItems', products_no_banco_de_dados)
        } else {
          context.commit('setAllItems', products_local_storage)
        }
      },
      show(){},
      create(){},
      update(){},
      destroy(){},
      
  
    //   indexTEST(context, filter) {
    //     Api.Item.index(filter)
    //       .then(response => response.data)
    //       .then(items => {
    //         context.commit('setAllItems', items);
    //         context.commit('indexResponse', {msg: '', was_good: true});
    //       }).catch(function(error) {
    //         context.commit('indexResponse', {msg: error.response.data.errors[0], was_good: false});
    //         console.log(error);
    //       });
    //   },
    }
  
  };
  
  export default Product;