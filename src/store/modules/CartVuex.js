// import Api from '../../api/index'

const Cart = {
    namespaced: true,
    state: {
      allCarts: null, // [],
      approved:null,
      oneCart: {
        id: 1,
        owner: 'fasdf',
        items: []
      }, // {}
      close_carts: {
        items:[]
      },
      success: null,
      success_msg: null,
      error: null,
      error_msg: null,
    },
    getters: {
      getAllCarts: state => {
        return state.allCarts;
      },
      getOneCartItems: state => {
        return state.oneCart.items;
      },
      getCloseCartsItems: state =>{
        return state.close_carts
      }
    },
  
    mutations: {
      addItem(state, item){
        if (state.oneCart.items.indexOf(item) == -1) {
          state.oneCart.items.push(item) // item.data
          localStorage.setItem('one_cart', JSON.stringify(state.oneCart))
        } else {
          alert('Esse valor já foi adicionado ao carrinho.')
        }
      },
      removeItem(state, item) {
        let index = state.oneCart.items.indexOf(item)
        state.oneCart.items.splice(index, 1)
        let aux_storage = JSON.parse(localStorage.getItem('one_cart'))
        aux_storage.items.splice(index, 1)
        localStorage.setItem('one_cart', JSON.stringify(aux_storage))
      },
      setOneCart(state, item) {
        if (localStorage.lenght > 1) {
          state.oneCart = item
          localStorage.setItem('one_cart', JSON.stringify(state.oneCart))
          console.log(state.oneCart)
        } else {
          localStorage.setItem('one_cart', JSON.stringify(state.oneCart))
        }
      },
      approveItem(state, item){
        state.close_carts = JSON.parse(localStorage.getItem('close_carts'))
        for(let n=0;n<state.close_carts.items.length;n++){
          if(state.close_carts.items[n].name == item.name){
            state.close_carts.items[n].approved = !state.close_carts.items[n].approved
            item.approved = !item.approved
            break 
          }
        }        
      }
    },
  
    actions: {
      setOneItem(context, item) {
        context.commit('addItem', item)
      },
      removeOneItem(context, item) {
        context.commit('removeItem', item)
      },
  
      index(context){
        let one_cart_local_storage = JSON.parse(localStorage.getItem('one_cart'))
        context.commit('setOneCart', one_cart_local_storage)
      },
      approveOneItem(context, item) {
        context.commit('approveItem', item)
      },
      show(){},
      create(){},
      update(){},
      destroy(){},
    }
  
  };
  
  export default Cart;