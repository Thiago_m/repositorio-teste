import Vue from 'vue'
import Vuex from 'vuex'

import Product from './modules/ProductVuex'
import Cart from './modules/CartVuex'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Product,
    Cart,
  }
})
