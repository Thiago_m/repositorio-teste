import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/add',
    name: 'AddProduct',
    component: () => import(/* webpackChunkName: "add_product" */ '../views/AddProduct.vue')
  },
  {
    path: '/Verify',
    name: 'VerifyCart',
    component: () => import(/* webpackChunkName: "add_product" */ '../views/VerifyCart.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
